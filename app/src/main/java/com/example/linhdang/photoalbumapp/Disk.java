package com.example.linhdang.photoalbumapp;

import android.content.Context;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;

/**
 * @author linhdang
 */

public class Disk implements Serializable {
    /**
     * Get Serialize version
     */
    private static final long getSerialVersionUID = 232142142142141L;
    /**
     * List of Albums
     */
    public ArrayList<Album> albums;
    /**
     * Serial version
     */
    private static final long serialVersionUID = 0L;

    /**
     * loadData from serialized file
     * @param context
     * @return
     */
    public static Disk loadData(Context context) {
        Disk disk = null;
        try {
            FileInputStream file = context.openFileInput("data.ser");
            try (ObjectInputStream os = new ObjectInputStream(file)) {
                disk = (Disk) os.readObject();
                if (disk.albums == null) {
                    disk.albums = new ArrayList<Album>();
                }
                try {
                    file.close();
                } catch (IOException e) {
                    return null;
                }
                try {
                    os.close();
                } catch (IOException e) {
                    return null;
                }
            } catch (IOException e) {
                return null;
            } catch (ClassNotFoundException e) {
                return null;
            }
        } catch (FileNotFoundException e) {
            return null;
        }
        return disk;
    }

    /**
     * Save Data to serialized file
     * @param context
     */
    public void SaveData(Context context) {
        FileOutputStream file = null;
        try {
            file = context.openFileOutput("data.ser", Context.MODE_PRIVATE);


            ObjectOutputStream obj = new ObjectOutputStream(file);
            obj.writeObject(this);
            obj.close();
            file.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}





