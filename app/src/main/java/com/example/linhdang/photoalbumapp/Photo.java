package com.example.linhdang.photoalbumapp;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Hashtable;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * @author linhdang
 */
public class Photo implements Serializable {
	/**
	 * File Name for Photo
	 */
	public String filename;
	/**
	 * Caption for Photo
	 */
	public String Caption;
	public String AbsolutePath;
	/**
	 * List of locations for photo
	 */
	ArrayList<String> LocationList = new ArrayList<String>();
	/**
	 * List of people for photo
	 */
	ArrayList<String> PersonList = new ArrayList<String>();
	/**
	 * BAD INPUT flag
	 */
	private static final int BADINPUT = -1;
	/**
	 * BAD TAG flag
	 */
	private static final int BADTAG = -2;

	/**
	 * Bitmap to not be serialized
	 */
	transient Bitmap bm;

	/**
	 * Last date modified for photo
	 */
	public long lastmodifieddate;
	/**
	 * List of tags
	 */
	public ArrayList<Tag> Taglist ;
	/**
	 * 
	 */
	private static final long serialVersionUID = 121214214214L;
	/**
	 * Simple Date Format
	 */
	private static final String SimpleDateFormat = null;

	/**
	 * Create Photo instance given caption and the data bitmap
	 * @param caption
	 * @param bit
	 */
	public Photo(String caption, Bitmap bit )  {


		bm = bit;
		this.filename = caption;
		this.Caption = caption;
		Taglist = new ArrayList<Tag>();

	}

	/**
	 * toString
	 */
	public String toString() {
		return getFilename();
	}
	/**
	 * Add Caption 
	 * @param string Value of Caption
	 */
	public void AddCaption(String string ) {
		this.Caption = string;
	}
	/**
	 * Get caption
	 * @return caption
	 */
	public String getCaption() {
		return this.Caption;
	}
	/**
	 * get image 
	 * @return image serialized image
	 */
	public Bitmap getImage() {
		return bm;
	}

	/**
	 * Set New Caption
	 * @param newcaption
	 */
	public void SetCaption(String newcaption){
		this.Caption = newcaption;
	}

	/**
	 * Get Tag List in String Form
	 * @return
	 */
	public String getTagListString(){
		String taglist="";
		if(Taglist.size() > 0){
			 taglist="";
			for(Tag t: Taglist){
				taglist= taglist+ t.toString()+", ";
			}
			taglist = taglist.substring(0,taglist.length() - 2);
		}
		return taglist;
	}

	/**
	 * Get Tag List
	 * @return
	 */
	public ArrayList<Tag> getTaglist(){
		return Taglist;
	}
	public String PhotoDetails(){
		String s = "Caption: "+ this.getCaption() + "\n"+ "Tag List: " + getTagListString();
		return s;
	}

	/**
	 * Get Filename
	 * @return
	 */
	public String getFilename(){
		return this.filename;
	}

	/**
	 * Add another Location to location list
	 * @param location
	 */
	public void toLocationList(String location){
		LocationList.add(location);

	}

	/**
	 * Get Location list
	 * @return
	 */
	public ArrayList<String> getLocationList(){
		return LocationList;
	}

	/**
	 * All person to person list
	 * @param person
	 */
	public void toPersonList(String person){
		PersonList.add(person);
	}

	/**
	 * Get Person list
	 * @return
	 */
	public ArrayList<String> getPersonList(){
		return PersonList;
	}

	/**
	 * Read a bitmap for photo
	 * @param in
	 * @throws IOException
	 */
	private void readObject(ObjectInputStream in) throws IOException {
		try {
			in.defaultReadObject();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		int b;
		ByteArrayOutputStream bytestream = new ByteArrayOutputStream();
		while((b = in.read()) != -1){
			bytestream.write(b);
		}
		byte bytes[] = bytestream.toByteArray();
		bm = BitmapFactory.decodeByteArray(bytes,0,bytes.length);


	}

	/**
	 * Write a bitmap from photo
	 * @param out
	 * @throws IOException
	 */
	private  void writeObject(ObjectOutputStream out) throws IOException {
		out.defaultWriteObject();
		if(bm != null){
			ByteArrayOutputStream bytestream = new ByteArrayOutputStream();
			bm.compress(Bitmap.CompressFormat.PNG,0,bytestream);
			byte bytes[] = bytestream.toByteArray();
			out.write(bytes,0,bytes.length);
		}
	}
}
