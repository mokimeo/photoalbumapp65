package com.example.linhdang.photoalbumapp;

import android.annotation.TargetApi;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * @author linhdang
 */
public class DisplayPhoto extends AppCompatActivity implements View.OnClickListener {
    /**
     * Image to display
     */
    ImageView image;
    /**
     * data about image
     */
    TextView metadata;
    /**
     * Buttons
     */
    Button addtag,next,previous,deletetag,edittag;
    /**
     * Tag Name Text
     */
    EditText TagName;
    /**
     * Tag Value Text
     */
    EditText TagValue;
    /**
     * Selected Photo
     */
    Photo SelectedPhoto;
    /**
     * List View for photos
     */
    private ListView listview;
    /**
     * Current index of photo
     */
    public int CurrentIndex;
    /**
     * Photo List
     */
    public ArrayList<Photo> CurrentPhotoList;
    /**
     * Index
     */
    int index;
    /**
     * Tag Index
     */
    int tagindex;
    /**
     * Current Context
     */
    Context context = this;

    /**
     * Start Display Photo Activity
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.display);
        String path = this.getApplicationInfo().dataDir;
        System.out.println("Path name:"+ path);
        Intent intent = getIntent();
        CurrentIndex = intent.getIntExtra("PhotoIndex",0);
        image = (ImageView) findViewById(R.id.imagedisplay);
        SelectedPhoto = MainActivity.disk.albums.get(OpenAlbum.currentAlbumindex).getPhotoList().get(CurrentIndex);
        CurrentPhotoList = OpenAlbum.currentAlbum.getPhotoList();
        ArrayAdapter<Tag> TagAdapter = new ArrayAdapter<Tag>(this,R.layout.tagview,SelectedPhoto.getTaglist());
        TagAdapter.setNotifyOnChange(true);
        listview = findViewById(R.id.TagListView);
        listview.setAdapter(TagAdapter);
        image.setImageBitmap(SelectedPhoto.getImage());
        metadata = (TextView) findViewById(R.id.textview);
        metadata.setText(SelectedPhoto.getFilename());
        addtag = (Button) findViewById(R.id.AddTag);
        next = (Button) findViewById(R.id.Next);
        previous= (Button) findViewById(R.id.Previous);
        addtag.setOnClickListener(this);
        next.setOnClickListener(this);
        previous.setOnClickListener(this);
        deletetag = (Button) findViewById(R.id.DeleteTag);
        deletetag.setOnClickListener(this);
        edittag = (Button) findViewById(R.id.EditTag);
        edittag.setOnClickListener(this);
        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                view.setSelected(true);
                tagindex = position;

            }
        });

    }

    /**
     * Add Tag Function
     */
    public void AddTag(){

        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View view = inflater.inflate(R.layout.addtag,
                null, false);

        final ArrayAdapter<Tag> tagadapter = (ArrayAdapter)listview.getAdapter();

        final RadioGroup tagchoice = (RadioGroup) view
                .findViewById(R.id.TagName);

        final EditText nameEditText = (EditText) view
                .findViewById(R.id.TagValue);

       final  AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(view).setTitle("Add Tag");
        builder.setPositiveButton("Add", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String toastString = "";

                int selectedId = tagchoice
                        .getCheckedRadioButtonId();
                System.out.println("choice button:"+ selectedId);
                if(selectedId < 0){
                    new AlertDialog.Builder(builder.getContext()).setMessage("Please Choose Tag Name!").setPositiveButton("ok",null).show();
                    return;
                }
                RadioButton selectedRadioButton = (RadioButton) view
                        .findViewById(selectedId);
                if(nameEditText.getText().toString().matches("")){
                    new AlertDialog.Builder(builder.getContext()).setMessage("Please fill out the tag value!").setPositiveButton("ok",null).show();
                    return;
                }
                toastString += "Selected radio button is: "
                        + selectedRadioButton.getText().toString() + "!\n";

                toastString += "Name is: " + nameEditText.getText().toString()
                        + "!\n";

                Tag newtag = new Tag(selectedRadioButton.getText().toString(),nameEditText.getText().toString());
                if(isDuplicatedTag(newtag,CurrentPhotoList.get(CurrentIndex).getTaglist())){
                    new AlertDialog.Builder(builder.getContext()).setMessage("This Tag Value already exists").setPositiveButton("ok",null).show();
                    return;
                }
                Photo currentphoto = CurrentPhotoList.get(CurrentIndex);
                if(selectedRadioButton.getText().toString().equals("Location")){
                    currentphoto.toLocationList(nameEditText.getText().toString());
                }else{
                    currentphoto.toPersonList(nameEditText.getText().toString());

                }
                for(int i = 0; i < currentphoto.getLocationList().size(); i++){
                    System.out.println(currentphoto.filename+","+"Location: "+ currentphoto.getLocationList().get(i));
                }
                for(int i = 0; i < currentphoto.getPersonList().size(); i++){
                    System.out.println(currentphoto.filename+","+"Person: "+ currentphoto.getPersonList().get(i));
                }
                tagadapter.add(newtag);
                MainActivity.disk.SaveData(context);

                System.out.println("Choice is:" + toastString);
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        final AlertDialog ad = builder.create();
        ad.show();

    }

    /**
     * Switch buttons
     * @param v
     */
    public void SwitchButton(View v){

            index = CurrentIndex;

            int ListLength = OpenAlbum.currentAlbum.getPhotoList().size();

            System.out.println(ListLength);
            if (v.getId() == R.id.Next) {
                if(index < ListLength - 1 ) {
                    index++;

                }else {
                    index = 0;
                }
            } else {
                if (index > 0) {
                    index--;
                } else {
                    index = ListLength - 1;
                }
            }


            Photo selectedphoto = OpenAlbum.currentAlbum.getPhotoList().get(index);

            CurrentIndex = index;
            ArrayAdapter<Tag> TagAdapter = new ArrayAdapter<>(this,R.layout.tagview,selectedphoto.getTaglist());
            listview.setAdapter(TagAdapter);
            image.setImageBitmap(selectedphoto.getImage());
            metadata.setText(selectedphoto.getFilename());


    }

    /**
     * Checks if the tag is duplicate
     * @param t
     * @param taglist
     * @return
     */
    public boolean isDuplicatedTag(Tag t, ArrayList<Tag> taglist){
        for(int i = 0; i < taglist.size(); i++){
            if(t.toString().equals(taglist.get(i).toString())){
                return true;
            }
        }
        return false;

    }

    /**
     * Delete Tag from Photo
     */
    public void DeleteTag(){

        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Delete Tag:");
        builder.setIcon(R.drawable.ic_launcher_background);
        builder.setMessage("Are you sure to delete:");
        ArrayAdapter<Tag> TagAdapter = (ArrayAdapter) listview.getAdapter();
        if(TagAdapter.getCount() == 0){
            return;
        }

        builder.setPositiveButton("Okay", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ArrayAdapter<Tag> TagAdapter = (ArrayAdapter) listview.getAdapter();
                Photo currentphoto = CurrentPhotoList.get(CurrentIndex);
                Tag selectedtag = TagAdapter.getItem(tagindex);
                
                if(selectedtag.TagName.equals("Location")){
                    currentphoto.getLocationList().remove(selectedtag.TagValue);
                }else{
                    currentphoto.getPersonList().remove(selectedtag.TagValue);
                }

                TagAdapter.remove(TagAdapter.getItem(tagindex));
                TagAdapter.notifyDataSetChanged();
                MainActivity.disk.SaveData(context);


            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        final AlertDialog ad = builder.create();
        ad.show();
    }

    /**
     * Edit Tag for Photo
     */
    public void EditTag(){

        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View view = inflater.inflate(R.layout.addtag,
                null, false);

        final ArrayAdapter<Tag> tagadapter = (ArrayAdapter)listview.getAdapter();

        final RadioGroup tagchoice = (RadioGroup) view
                .findViewById(R.id.TagName);

        final EditText nameEditText = (EditText) view
                .findViewById(R.id.TagValue);

        final  AlertDialog.Builder builder = new AlertDialog.Builder(this);
        if(tagadapter.getCount() <= 0){
            new AlertDialog.Builder(builder.getContext()).setMessage("There is not tag to edit!").setPositiveButton("ok",null).show();
            return;
        }
        builder.setView(view).setTitle("Edit Tag");
        builder.setPositiveButton("Edit", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String toastString = "";

                int selectedId = tagchoice
                        .getCheckedRadioButtonId();
                System.out.println("choice button:"+ selectedId);
                if(selectedId < 0){
                    new AlertDialog.Builder(builder.getContext()).setMessage("Please Choose Tag Name!").setPositiveButton("ok",null).show();
                    return;
                }
                RadioButton selectedRadioButton = (RadioButton) view
                        .findViewById(selectedId);
                if(nameEditText.getText().toString().matches("")){
                    new AlertDialog.Builder(builder.getContext()).setMessage("Please fill out the tag value!").setPositiveButton("ok",null).show();
                    return;
                }
                toastString += "Selected radio button is: "
                        + selectedRadioButton.getText().toString() + "!\n";

                toastString += "Name is: " + nameEditText.getText().toString()
                        + "!\n";

                Tag newtag = new Tag(selectedRadioButton.getText().toString(),nameEditText.getText().toString());

                if(isDuplicatedTag(newtag,CurrentPhotoList.get(CurrentIndex).getTaglist())){
                    new AlertDialog.Builder(builder.getContext()).setMessage("This Tag Value already exists").setPositiveButton("ok",null).show();
                    return;
                }
                Photo currentphoto = CurrentPhotoList.get(CurrentIndex);

                if(tagadapter.getItem(tagindex).TagName.equals("Location")){
                    currentphoto.getLocationList().remove(tagadapter.getItem(tagindex).TagValue);

                }else{
                    currentphoto.getPersonList().remove(tagadapter.getItem(tagindex).TagValue);


                }
                tagadapter.remove(tagadapter.getItem(tagindex));
                tagadapter.notifyDataSetChanged();

                tagadapter.add(newtag);

                if(selectedRadioButton.getText().toString().equals("Location")){
                    currentphoto.toLocationList(nameEditText.getText().toString());
                }else{
                    currentphoto.toPersonList(nameEditText.getText().toString());

                }
                for(int i = 0; i < currentphoto.getLocationList().size(); i++){
                    System.out.println(currentphoto.filename+","+"Location: "+ currentphoto.getLocationList().get(i));
                }
                for(int i = 0; i < currentphoto.getPersonList().size(); i++){
                    System.out.println(currentphoto.filename+","+"Person: "+ currentphoto.getPersonList().get(i));
                }
                MainActivity.disk.SaveData(context);

            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        final AlertDialog ad = builder.create();
        ad.show();
    }

    /**
     * Handler for Button Clicks
     * @param v
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.AddTag:
                AddTag();
                break;
            case R.id.Next:
                SwitchButton(v);

                break;
            case R.id.Previous:
                SwitchButton(v);
                break;
            case R.id.DeleteTag:
                DeleteTag();
                break;
            case R.id.EditTag:
                EditTag();
                break;
        }
    }
}
