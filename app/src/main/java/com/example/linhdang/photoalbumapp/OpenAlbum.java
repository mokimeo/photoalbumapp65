package com.example.linhdang.photoalbumapp;

import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.ParcelFileDescriptor;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * @author linhdang
 */
public class OpenAlbum extends AppCompatActivity implements View.OnClickListener  {
    /**
     * Used to organize data
     */
    public static GridView gridview;
    /**
     * Toolbar
     */
    TextView toolbar;
    /**
     * Buttons
     */
    Button add,display,caption,delete,move,copy;
    /**
     * Current Album
     */
    public static Album currentAlbum;
    /**
     * Image Adapter
     */
    public static ImageAdapter imageAdapter;
    /**
     * The Input
     */
    EditText input;
    /**
     * Index select for album
     */
    int indexSelected;
    /**
     * Current Context
     */
    Context context = this;
    /**
     * List of albums
     */
    ArrayList<Album> albumlist;
    /**
     * Current Album Index
     */
    public static int currentAlbumindex;
    /**
     * previous delete index
     */
    public int previousindex;
    /**
     * Start Opened Album Activity
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.openalbum);
        Intent intent = getIntent();
        int currentindex = intent.getIntExtra("Albumindex",0);
        Log.d("Album index: ", ""+currentindex);

        currentAlbum = MainActivity.disk.albums.get(currentindex);
        currentAlbumindex = currentindex;
        imageAdapter = new ImageAdapter(this,currentAlbum.getPhotoList());
        gridview = findViewById(R.id.gv);
        gridview.setAdapter(imageAdapter);

        add = (Button)findViewById(R.id.Add);
        add.setOnClickListener(this);
        caption = (Button) findViewById(R.id.Caption);
        caption.setOnClickListener(this);
        delete = (Button) findViewById(R.id.Delete);
        delete.setOnClickListener(this);
        display = (Button) findViewById(R.id.Display);
        display.setOnClickListener(this);
        move = (Button) findViewById(R.id.Move);
        move.setOnClickListener(this);
        copy = (Button) findViewById(R.id.Copy);
        copy.setOnClickListener(this);
        gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                view.setSelected(true);
                indexSelected = position;


            }
        });



    }

    /**
     * Create Options Menu
     * @param menu
     * @return
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    /**
     * Add Photo Function
     */
    public void AddPhoto(){
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        startActivityForResult(intent,1);

    }

    /**
     *  Activity result for image
     * @param requestCode
     * @param resultCode
     * @param imageReturnIntent
     */
    @Override
    protected  void onActivityResult(int requestCode, int resultCode, Intent imageReturnIntent){
        super.onActivityResult(requestCode,resultCode,imageReturnIntent);
        Log.d("num", "onActivityResult: "+ resultCode);

        if(resultCode == RESULT_OK){
            Uri image = imageReturnIntent.getData();
            String path = image.getPath();
            final AlertDialog.Builder builder = new AlertDialog.Builder(this);


            builder.setIcon(R.drawable.ic_launcher_background);


            ImageView iv = new ImageView(this);

            iv.setImageURI(image);
            Bitmap bm= ((BitmapDrawable) iv.getDrawable()).getBitmap();
            Photo photo = new Photo(path,bm);
            if(isDuplicatedPhoto(photo,currentAlbum.getPhotoList())){
                new AlertDialog.Builder(builder.getContext()).setMessage("Item already exists").setPositiveButton("ok",null).show();
                return;
            }

            currentAlbum.getPhotoList().add(photo);


            imageAdapter = (ImageAdapter)gridview.getAdapter();


            gridview.setAdapter(imageAdapter);
            MainActivity.disk.SaveData(context);




        }
    }

    /**
     * Check if photo is duplicated
     * @param photo
     * @param photolist
     * @return
     */
    public boolean isDuplicatedPhoto(Photo photo, ArrayList<Photo> photolist){
        for(Photo p: photolist){
            if(p.getFilename().equals(photo.getFilename())){
                return true;
            }
        }
        return false;
    }

    /**
     * Add Caption to photo
     */
    public void AddCaption(){
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setTitle("Edit Caption");
        builder.setIcon(R.drawable.ic_launcher_foreground);
        builder.setMessage("Please Enter Caption:");
        input = new EditText(this);
        input.setSelection(input.getText().length());
        builder.setView(input);


        builder.setPositiveButton("Submit", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String txt = input.getText().toString();

                OpenAlbum.currentAlbum.getPhotoList().get(indexSelected).SetCaption(txt);
                imageAdapter.getItem(indexSelected).SetCaption(txt);
                imageAdapter.notifyDataSetChanged();
                MainActivity.disk.SaveData(context);
                Toast.makeText(getApplicationContext(),txt,Toast.LENGTH_LONG).show();

            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        final AlertDialog ad = builder.create();
        ad.show();
    }

    /**
     * Display Photo
     */
    public void DisplayPhoto(){
        Intent intent = new Intent(OpenAlbum.this, DisplayPhoto.class);
        intent.putExtra("PhotoIndex",indexSelected);
        startActivity(intent);
    }

    /**
     * Delete Photo
     */
    public void DeletePhoto(){


            final AlertDialog.Builder builder = new AlertDialog.Builder(this);

            builder.setTitle("Delete Photo");
            builder.setIcon(R.drawable.ic_launcher_foreground);
            builder.setMessage("Are you sure to delete:");

            if(imageAdapter.getCount() == 0){
                return;
            }

            builder.setPositiveButton("Okay", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    System.out.println("Delete:" + indexSelected);
                    System.out.println("Num of photo: " + imageAdapter.getCount());
                    if(indexSelected >= imageAdapter.getCount()){
                        new AlertDialog.Builder(builder.getContext()).setMessage("Please Select Photo to delete!").setPositiveButton("ok",null).show();
                        return;
                    }
                    currentAlbum.getPhotoList().remove(currentAlbum.getPhotoList().get(indexSelected));
                    imageAdapter = (ImageAdapter)gridview.getAdapter();


                    gridview.setAdapter(imageAdapter);
                    MainActivity.disk.SaveData(context);


                }
            });
            builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            final AlertDialog ad = builder.create();
            ad.show();

    }

    /**
     * Move Photo to another Album
     */
    public void MovePhoto(){
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        if(gridview.getCount() <= 0){
            new AlertDialog.Builder(builder.getContext()).setMessage("There is no photo to move !").setPositiveButton("ok",null).show();
            return;
        }
        ArrayList<String> albumtoString = new ArrayList<String>();
        for(Album a: MainActivity.disk.albums){
            if(!a.getName().equals(currentAlbum.getName())){
                albumtoString.add(a.getName());
            }
        }
        String[] album = new String[albumtoString.size()];
        final String[] albums = albumtoString.toArray(album);


        final ArrayList<Integer> mSelectedItems = new ArrayList<Integer>();

        final Album albumdes = new Album(albums[0]);
        // Set the dialog title
        builder.setTitle("Choose One")

                .setSingleChoiceItems(albums, 0, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        albumdes.EditName(albums[arg1]);
                    }

                })

                // Set the action buttons
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        Photo selectedPhoto = imageAdapter.getItem(indexSelected);

                        for(Album a:MainActivity.disk.albums){
                            if(a.getName().equals(albumdes.getName())){
                                if(isDuplicatedPhoto(selectedPhoto,a.getPhotoList())){
                                    new AlertDialog.Builder(builder.getContext()).setMessage("Item already exists in "+a.getName() +"'s album").setPositiveButton("ok",null).show();
                                    return;
                                }
                                imageAdapter.remove(selectedPhoto);
                                a.getPhotoList().add(selectedPhoto);
                                MainActivity.disk.SaveData(context);

                            }

                        }

                    }
                })

                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();

                    }
                })

                .show();
    }

    /**
     * Copy Photo to another album
     */
    public void CopyPhoto(){
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        if(gridview.getCount() <= 0){
            new AlertDialog.Builder(builder.getContext()).setMessage("There is no photo to move !").setPositiveButton("ok",null).show();
            return;
        }
        ArrayList<String> albumtoString = new ArrayList<String>();
        for(Album a: MainActivity.disk.albums){
            if(!a.getName().equals(currentAlbum.getName())){
                albumtoString.add(a.getName());
            }
        }
        String[] album = new String[albumtoString.size()];
        final String[] albums = albumtoString.toArray(album);


        final ArrayList<Integer> mSelectedItems = new ArrayList<Integer>();

        final Album albumdes = new Album(albums[0]);
        // Set the dialog title
        builder.setTitle("Choose One")

                .setSingleChoiceItems(albums, 0, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        albumdes.EditName(albums[arg1]);
                    }

                })

                // Set the action buttons
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        Photo selectedPhoto = imageAdapter.getItem(indexSelected);
                        for(Album a:MainActivity.disk.albums){
                            if(a.getName().equals(albumdes.getName())){
                                if(isDuplicatedPhoto(selectedPhoto,a.getPhotoList())){
                                    new AlertDialog.Builder(builder.getContext()).setMessage("Item already exists in "+a.getName() +"'s album").setPositiveButton("ok",null).show();
                                    return;
                                }
                                a.getPhotoList().add(selectedPhoto);
                                MainActivity.disk.SaveData(context);

                            }

                        }

                    }
                })

                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();

                    }
                })

                .show();
    }

    /**
     * Handle all button clicks
     * @param v
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.Add:
                AddPhoto();
                break;
            case R.id.Delete:
                DeletePhoto();
                break;
            case R.id.Caption:
                AddCaption();
                break;
            case R.id.Move:
                MovePhoto();
                break;
            case R.id.Display:
                DisplayPhoto();
                break;

            case R.id.Copy:
                CopyPhoto();
                break;
        }
    }
}
