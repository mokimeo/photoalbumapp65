package com.example.linhdang.photoalbumapp;

import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * @author linhdang
 */
public class SearchPhoto extends AppCompatActivity implements View.OnClickListener  {
    /**
     * Search button
     */
    Button search;
    /**
     * Grid to view
     */
    GridView gridview;
    /**
     * Tag Value
     */
    EditText tagvalue;
    /**
     * Tag Radio
     */
    RadioGroup tagRadio;

    /**
     * Start Photo Search activity
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.photosearch);
        gridview = (GridView) findViewById(R.id.searchgridview);
        tagvalue= (EditText) findViewById(R.id.tagvaluesearch);

        tagRadio = (RadioGroup) findViewById(R.id.tagchoice);
        search = (Button) findViewById(R.id.Search);
        search.setOnClickListener(this);
    }

    /**
     * Search Photo
     */
    public void SearchPhoto(){
        gridview = (GridView) findViewById(R.id.searchgridview);
        ArrayList<Photo> selectedPhotos = new ArrayList<Photo>();
        String toastString = "";
        final  AlertDialog.Builder builder = new AlertDialog.Builder(this);
        int selectedId = tagRadio
                .getCheckedRadioButtonId();
        System.out.println("choice button:"+ selectedId);
        if(selectedId < 0){
            new AlertDialog.Builder(builder.getContext()).setMessage("Please Choose Tag Name!").setPositiveButton("ok",null).show();
            return;
        }
        RadioButton selectedRadioButton = (RadioButton) findViewById(selectedId);
        if(tagvalue.getText().toString().matches("")){
            new AlertDialog.Builder(builder.getContext()).setMessage("Please fill out the tag value!").setPositiveButton("ok",null).show();
            return;
        }
        toastString += "Selected radio button is: "
                + selectedRadioButton.getText().toString() + "!\n";

        toastString += "Name is: " + tagvalue.getText().toString()
                + "!\n";

        Tag tag_target = new Tag(selectedRadioButton.getText().toString(),tagvalue.getText().toString());

        String tname = selectedRadioButton.getText().toString();
        String tvalue = tagvalue.getText().toString();
        System.out.println("tag is:" + tag_target);
        ArrayList<Album> AlbumList = MainActivity.disk.albums;
        if(tname.equals("Location")){
                for(int i = 0; i < AlbumList.size(); i++){
                    for(int j = 0; j < AlbumList.get(i).getPhotoList().size(); j++){
                            ArrayList<String> location = AlbumList.get(i).getPhotoList().get(j).getLocationList();
                            for (int k = 0; k < location.size(); k++){
                                if(location.get(k).contains(tvalue)){
                                    if (getPhotoString(selectedPhotos).contains(AlbumList.get(i).getPhotoList().get(j).getFilename())) {
                                        continue;
                                    } else {
                                        selectedPhotos.add(AlbumList.get(i).getPhotoList().get(j));

                                    }

                                }
                                System.out.println("from photo name"+AlbumList.get(i).getPhotoList().get(j).getFilename()+"**location: "+location.get(k));


                            }


                    }
                }

        }else{
            for(int i = 0; i < AlbumList.size(); i++){
                for(int j = 0; j < AlbumList.get(i).getPhotoList().size(); j++){
                    ArrayList<String> person = AlbumList.get(i).getPhotoList().get(j).getPersonList();
                    for (int k = 0; k < person.size(); k++){
                        if(person.get(k).contains(tvalue)){
                            if (getPhotoString(selectedPhotos).contains(AlbumList.get(i).getPhotoList().get(j).getFilename())) {
                                continue;
                            } else {
                                selectedPhotos.add(AlbumList.get(i).getPhotoList().get(j));

                            }

                        }
                        System.out.println("from photo name"+AlbumList.get(i).getPhotoList().get(j).getFilename()+"**Person "+person.get(k));
                    }


                }
            }
        }
        ImageAdapter imageadapter = new ImageAdapter(this,selectedPhotos);
        gridview.setAdapter(imageadapter);



    }

    /**
     * Get Photo String
     * @param photolist
     * @return
     */
    public ArrayList<String> getPhotoString(ArrayList<Photo> photolist){
        ArrayList<String> arr = new ArrayList<String>();
        for(Photo photo:photolist){
            arr.add(photo.toString());

        }
        return arr;
    }


    /**
     * On Click
     * @param v
     */
    @Override
    public void onClick(View v) {
            switch (v.getId()) {
                case R.id.Search:
                    SearchPhoto();
                    break;
            }
        }
}
