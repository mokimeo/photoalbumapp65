package com.example.linhdang.photoalbumapp;

import java.io.Serializable;
import java.util.*;
import java.util.function.BiFunction;

/**
 * 
 * @author linhdang
 *
 */
public class Tag implements Serializable {
	/**
	 * Tag Name
	 */
	String TagName;
	/**
	 * Tag Value
	 */
	String TagValue;

	/**
	 * Create Tag
	 * @param tagname
	 * @param tagvalue
	 */
		public Tag(String tagname, String tagvalue){
				this.TagName = tagname;
				this.TagValue = tagvalue;


		}
		
		/**
		 * Return string format of Tag-Value Pair
		 */
		public String toString() {
		
		return this.TagName+"-"+this.TagValue;
		
		}


}
