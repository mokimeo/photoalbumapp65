package com.example.linhdang.photoalbumapp;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

/**
 * @author linhdang
 */
public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    /**
     * Used to view albums
     */
    ListView listView;
    /**
     * Used to detect if user wants to add album
     */
    Button addalbum;
    /**
     * Used to detect if user wants to delete album
     */
    Button DeleteAlbum;
    /**
     * Used to detect if user wants to rename album
     */
    Button RenameAlbum;
    /**
     * Used to detect if user wants to open album
     */
    Button OpenAlbum;
    /**
     * Used to detect if user wants to search for photos
     */
    Button SearchPhoto;
    /**
     * File used to serialize album
     */
    static String filename = "Album.ser";
    /**
     * Current index of album selected
     */
    public static int currentindex;
    /**
     * ArrayList containing Album objects
     */
    public static ArrayList<Album> albumArrayList = new ArrayList<Album>();
    /**
     * Array adapter which detects data changes in list
     */
    ArrayAdapter<Album>  arrayAdapter;
    /**
     *
     */
    String path;
    /**
     * Text input for album name
     */
    EditText input;
    /**
     * Context
     */
    final Context context = this;
    /**
     * Dist object
     */
    public static Disk disk;

    /**
     * Method override to start app
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        listView = (ListView) findViewById(R.id.listview);

        disk = Disk.loadData(this);
        if(disk == null){
            disk = new Disk();
        }
        if(disk.albums == null){
            System.out.println("Empty");
            disk.albums = new ArrayList<Album>();
            disk.albums.add(new Album("Stock"));
            disk.SaveData(context);

        }


        arrayAdapter = new ArrayAdapter<Album>(
                context,
                R.layout.album,
                disk.albums );

          arrayAdapter.setNotifyOnChange(true);
        listView.setAdapter(arrayAdapter);
        addalbum = (Button)findViewById(R.id.addalbum);
        addalbum.setOnClickListener(this);
        DeleteAlbum = (Button) findViewById(R.id.DeleteAlbum);
        DeleteAlbum.setOnClickListener(this);
        RenameAlbum = (Button) findViewById(R.id.RenameAlbum);
        RenameAlbum.setOnClickListener(this);
        OpenAlbum = (Button) findViewById(R.id.OpenAlbum);
        OpenAlbum.setOnClickListener(this);
        SearchPhoto = (Button) findViewById(R.id.SearchPhoto);
        SearchPhoto.setOnClickListener(this);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                view.setSelected(true);
                currentindex = position;

            }
        });




    }

    /**
     * Function for adding an album
     */
    public void AddAlbum(){
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setTitle("Add Album");
        builder.setIcon(R.drawable.ic_launcher_background);
        builder.setMessage("Please Enter Album Name:");
         input = new EditText(this);
        builder.setView(input);
        builder.setPositiveButton("Submit", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String txt = input.getText().toString();
                Album newalbum = new Album(txt);
                for(int i = 0; i < arrayAdapter.getCount(); i++){
                    Log.d("Album", ""+arrayAdapter.getItem(i));
                        if(newalbum.getName().equals(arrayAdapter.getItem(i).getName())){
                            new AlertDialog.Builder(builder.getContext()).setMessage("Item already exists").setPositiveButton("ok",null).show();
                            return;
                        }
                }

                arrayAdapter.add(newalbum);
                Toast.makeText(getApplicationContext(),txt,Toast.LENGTH_LONG).show();
                disk.SaveData(context);

            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        final AlertDialog ad = builder.create();
        ad.show();
    }

    /**
     * Function for renaming an album
     */
    public void RenameAlbum(){
        Log.d("Index", ""+ currentindex);
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setTitle("Rename Album");
        builder.setIcon(R.drawable.ic_launcher_background);
        builder.setMessage("Please Enter Album Name:");
        input = new EditText(this);
        input.setSelection(input.getText().length());
        builder.setView(input);
        if(arrayAdapter.getCount() <= 0){
            new AlertDialog.Builder(builder.getContext()).setMessage("There is no Album to ReName !").setPositiveButton("ok",null).show();
            return;
        }

        builder.setPositiveButton("Submit", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String txt = input.getText().toString();

                for(int i = 0; i < arrayAdapter.getCount(); i++){
                    Log.d("Album", ""+arrayAdapter.getItem(i));
                    if(txt.equals(arrayAdapter.getItem(i).name)){
                        new AlertDialog.Builder(builder.getContext()).setMessage("Item already exists").setPositiveButton("ok",null).show();
                        return;
                    }
                }

                arrayAdapter.getItem(currentindex).EditName(txt);

                disk.SaveData(context);
                arrayAdapter.notifyDataSetChanged();
                Toast.makeText(getApplicationContext(),txt,Toast.LENGTH_LONG).show();

            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        final AlertDialog ad = builder.create();
        ad.show();
    }

    /**
     * Function for deleting an album
     */
    public void DeleteAlbum(){
        Log.d("Index", ""+ currentindex);
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);

        builder.setTitle("Delete Album");
        builder.setIcon(R.drawable.ic_launcher_background);
        builder.setMessage("Are you sure to delete:");

        if(arrayAdapter.getCount() == 0){
            return;
        }

        builder.setPositiveButton("Okay", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                arrayAdapter.remove(arrayAdapter.getItem(currentindex));

                disk.SaveData(context);
                arrayAdapter.notifyDataSetChanged();


            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        final AlertDialog ad = builder.create();
        ad.show();
    }

    /**
     * Function for opening an album
     */
    public void OpenAlbum(){
        Intent intent = new Intent(MainActivity.this, OpenAlbum.class);
        intent.putExtra("Albumindex", currentindex);

        startActivity(intent);
    }

    /**
     * Function to start photo se
     */
    public void SearchPhoto(){
        Intent intent = new Intent(MainActivity.this, SearchPhoto.class);

        startActivity(intent);
    }

    /**
     * Function to handle a button click
     * @param v
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.addalbum:
                AddAlbum();
                break;
            case R.id.DeleteAlbum:
                DeleteAlbum();
                break;
            case R.id.RenameAlbum:
                RenameAlbum();
                break;
            case R.id.OpenAlbum:
                OpenAlbum();
                break;
            case R.id.SearchPhoto:
                SearchPhoto();
                break;
        }
    }

}
