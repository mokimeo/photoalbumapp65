package com.example.linhdang.photoalbumapp;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;
import java.util.ArrayList;

/**
 * @author linhdang
 */
public class ImageAdapter extends ArrayAdapter<Photo> {
    /**
     * Current Context
     */
    Context context;
    /**
     * Image to View
     */
    ImageView imageview;
    /**
     * List of Photos
     */
    ArrayList<Photo> photolist;

    /**
     * Initialize Image Adapter
     * @param context
     * @param photolist
     */
    public ImageAdapter(Context context, ArrayList<Photo> photolist) {
        super(context, R.layout.photo, photolist);
        this.context = context;
        this.photolist = photolist;

    }


    /**
     * Get View from given view
     * @param position
     * @param convertView
     * @param parent
     * @return
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        TextView caption;
        ViewHolder viewholder = null;
        Photo photo = (Photo) photolist.get(position);

        if (convertView == null) {
            LayoutInflater i = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = (View) i.inflate(R.layout.photo, parent, false);
            viewholder = new ViewHolder(convertView);
            convertView.setTag(viewholder);


        } else {
            viewholder = (ViewHolder) convertView.getTag();
        }
        viewholder.img.setImageBitmap(photo.getImage());
        viewholder.txt.setText("Caption: "+ photo.getCaption());
        return convertView;


    }

    class ViewHolder {
        TextView txt;
        ImageView img;

        ViewHolder(View v) {
            txt = (TextView) v.findViewById(R.id.caption);
            img = (ImageView) v.findViewById(R.id.imageview);
        }

    }
}


